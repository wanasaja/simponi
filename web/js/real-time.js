/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-26 22:08:36
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-21 14:17:11
*/

let vHost = window.location.hostname

const socket = io(vHost+':3000')
const url = window.location.host

//start 10
socket.on('replace_monitoring', function(data) {
    replaceTabelMonitoring()
})
socket.on('replace_unread_monitoring', function() {
    replaceUnreadMonitoring()
})
function replaceTabelMonitoring() {

    $.pjax.reload({container: '#pjax-tb-mon'});

    // $("#tb-mon").yiiGridView();

    // $.ajax({
    //     url: url + '/../../rujukan/monitoring-partial',
    //     method: 'get',
    //     success: function(data) {
    //         $('.tabel-monitoring').replaceWith(data)
    //     }
    // })
}
function replaceUnreadMonitoring() {
    $.ajax({
        url: url + '/../../rujukan/monitoring-notif',
        method: 'get',
        success: function(data) {
            $('.unread-monitoring').replaceWith(data)
        }
    })
}
//end 10



//start 20
socket.on('replace_riwayat_rujukan', function(data) {
    replaceTabelRiwayatRujukan()
})
function replaceTabelRiwayatRujukan() {
    $.pjax.reload({container: '#pjax-tb-riw'});

    // $.ajax({
    //     url: url + '/../../rujukan/riwayat-partial',
    //     methos: 'get',
    //     success: function(data) {
    //         $('.tabel-riwayat-rujukan').replaceWith(data)
    //     }
    // })
}
//end 20


//start Untuk Chat
socket.on('chat_update', function(data) {
    updateKotakChat();

    $('#kotak-chat').bind('pjax:end', function() {
      $('#kotak-chat').scrollTop($('#kotak-chat').prop("scrollHeight"));
    });
})
function updateKotakChat() {
    // $.pjax.defaults.timeout = false;
    $('.unread-pesan').load(' .unread-pesan');   
    $.pjax.reload({container: '#pjax-chat'});

}
//end Untuk Chat







// function replaceReadCounter() {
//     $.ajax({
//         url: url + "/../../rujukan/read-counter",
//         method: "get",
//         success: function(data) {
//             $(".read-counter").text(data)
//         }
//     })
// }

// function checkAsRead(id, url) {
//     $.ajax({
//         url: url,
//         method: "get",
//         success: function(data) {}
//     })
// }



// socket.on("replace_read_counter", function() {
//     replaceReadCounter()
// })

// $(".guestbook-list").on("click", ".view_message", function(e) {
//     e.preventDefault()
//     checkAsRead($(this).attr("value"), $(this).attr("href"))
// })
