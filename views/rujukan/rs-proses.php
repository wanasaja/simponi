<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-18 09:27:11
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-18 17:48:00
*/

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\Select2;


$this->title = 'Proses Rujukan';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="rujukan-proses">
    
    <?php $form = ActiveForm::begin([
            // 'id' => 'login-form-horizontal', 
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
        ]);
    ?>

    <div class="row">
        <div class="col-md-6">
        <div class="panel panel-primary">
              <div class="panel-heading">Data Umum Pasien</div>
              <div class="panel-body">
                
                <?php
                    $jenisPasien = [
                        'Ibu' => 'Ibu',
                        'Bayi' => 'Bayi'
                    ];
                    echo $form->field($model, 'jenis')->radioButtonGroup($jenisPasien, [
                        'disabledItems' => ['Ibu','Bayi'],
                        'class' => 'btn-group-md',
                        'itemOptions' => [
                            'labelOptions' => ['class' => 'jenisPasien btn btn-default'],
                            'id'=>'rujukan-jenis-item'
                        ],
                    ]);
                ?>
                <?= Html::activeHiddenInput($model, 'jenis', ['value' => $model->jenis]);  ?>
                
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
                
                <?php
                    $jk = [
                        'Laki-laki' => 'Laki-laki',
                        'Perempuan' => 'Perempuan'
                    ];
                    echo $form->field($model, 'jk',  ['options' => ['class' => 'form-group', 'id'=>'rujukan-jenis-group']])->radioButtonGroup($jk, [
                        'disabledItems' => ['Laki-laki','Perempuan'],
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                        'id' => 'fg-jk'
                    ]);
                ?>
                <?= Html::activeHiddenInput($model, 'jk', ['value' => $model->jk]);  ?>

                <div class="form-group field-rujukan-berat_bayi">
                    <label class="control-label col-sm-3" for="rujukan-berat_bayi">Berat Bayi</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'berat_bayi',[
                                    'addon' => [ 
                                        'append' => ['content' => 'gram', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput(['readonly'=>true])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'tplahir')->textInput(['maxlength' => true, 'readonly'=>true]) ?>

                <?php
                    echo $form->field($model, 'tglahir')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Pilih Tanggal Lahir', 'id'=>'date2', 'disabled'=>true],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            'endDate' => '+0d',
                            'todayHighlight' => true,
                            // 'todayBtn' => true,
                        ]
                    ]);
                ?>

                <?= $form->field($model, 'umur')->textInput(['maxlength' => true, 'readOnly'=> true]) ?>

                <?= $form->field($model, 'alamat')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?php
                    $caraBayar = [
                        'UMUM' => 'UMUM',
                        'BPJS' => 'BPJS',
                        'JKD' => 'JKD',
                        'JAMPERSAL' =>'JAMPERSAL'
                    ];
                    echo $form->field($model, 'cara_bayar')->widget(Select2::classname(), [
                        'data' => $caraBayar,
                        'disabled' => true,
                        'options' => [
                            'placeholder' => 'Pilih Cara Pembayaran...',
                        ],
                    ]);      
                ?>
                
                <div id="update-form-cara-bayar">
                    <?php //echo Html::activeHiddenInput($model, 'nik', ['value' => $model->nik]);  ?>
                    <?php //echo Html::activeHiddenInput($model, 'no_bpjs_jkd', ['value' => $model->no_bpjs_jkd]);  ?>

                    <?php echo $form->field($model, 'nik')->textInput(['maxlength' => true, 'readonly'=>true]); ?>

                    <?php echo $form->field($model, 'no_bpjs_jkd')->textInput(['readonly'=>true]); ?>
                    
                </div>

              </div>
          </div>
      </div>
        <div class="col-md-6">
        <div class="panel panel-primary">
              <div class="panel-heading">Rujukan & Resume Pasien</div>
              <div class="panel-body">

                <?= Html::activeHiddenInput($model, 'asal_rujukan', ['value' => $model->asal_rujukan]);  ?>
                <?= Html::activeHiddenInput($model, 'tujuan_rujukan', ['value' => $model->tujuan_rujukan]);  ?>

                <?php // $form->field($model, 'asal_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?php // $form->field($model, 'tujuan_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <div class="form-group field-rujukan-asal_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-asal_rujukan">Asal Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->asal_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="form-group field-rujukan-tujuan_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-tujuan_rujukan">Tujuan Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->tujuan_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <?= $form->field($model, 'alasan_rujukan')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?= $form->field($model, 'anamnesa')->textarea(['rows' => 6, 'readonly'=>true]) ?>
                
                <?php
                    $kesadaran = [
                        'Compos Mentis' => 'Compos Mentis',
                        'Somnolen' => 'Somnolen',
                        'Sopor' => 'Sopor',
                        'Comma' => 'Comma'
                    ];
                    echo $form->field($model, 'kesadaran')->radioButtonGroup($kesadaran, [
                        'disabledItems' => ['Compos Mentis','Somnolen','Sopor','Comma'],
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                    ]);
                ?>
                <?= Html::activeHiddenInput($model, 'kesadaran', ['value' => $model->kesadaran]);  ?>

                <div class="form-group field-rujukan-tekanan_darah">
                    <label class="control-label col-sm-3" for="rujukan-tekanan_darah">Tekanan Darah</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'tekanan_darah',[
                                    'addon' => [ 
                                        'append' => ['content' => 'mmHg', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput(['readonly'=>true])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-nadi">
                    <label class="control-label col-sm-3" for="rujukan-nadi">Nadi</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'nadi',[
                                    'addon' => [ 
                                        'append' => ['content' => 'x/menit', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput(['readonly'=>true])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-suhu">
                    <label class="control-label col-sm-3" for="rujukan-suhu">Suhu</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'suhu',[
                                    'addon' => [ 
                                        'append' => ['content' => 'C', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput(['readonly'=>true])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-pernapasan">
                    <label class="control-label col-sm-3" for="rujukan-pernapasan">Pernapasan</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'pernapasan',[
                                    'addon' => [ 
                                        'append' => ['content' => 'x/menit', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput(['readonly'=>true])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <?php
                    $nyeriOpsi = [
                        'Nyeri Berat' => 'Nyeri Berat',
                        'Nyeri Sedang' => 'Nyeri Sedang',
                        'Nyeri Ringan' => 'Nyeri Ringan'
                    ];
                    echo $form->field($model, 'nyeri')->radioButtonGroup($nyeriOpsi, [
                        'disabledItems' => ['Nyeri Berat', 'Nyeri Sedang', 'Nyeri Ringan'],
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                    ]);
                ?>
                <?= Html::activeHiddenInput($model, 'nyeri', ['value' => $model->nyeri]);  ?>

                <?= $form->field($model, 'pemeriksaan_fisik')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?= $form->field($model, 'pemeriksaan_penunjang')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?= $form->field($model, 'diagnosa')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?= $form->field($model, 'tindakan_yg_sdh_diberikan')->textarea(['rows' => 6, 'readonly'=>true]) ?>

                <?php
                    $statusOpsi = [
                        'Diterima' => 'Diterima',
                        'Tidak Diterima' => 'Tidak Diterima',
                    ];
                    echo $form->field($model, 'status')->radioButtonGroup($statusOpsi, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-success']],
                    ]);
                ?>
                <?= $form->field($model, 'info_balik')->textArea(['rows' => 6])?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?php 
                        echo Html::submitButton('Simpan', [
                            'class' => 'btn btn-primary',
                        ]) ?>
                    </div>
                </div>

              </div>
          </div>
      </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
