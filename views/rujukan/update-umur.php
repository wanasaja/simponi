<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-12 21:07:36
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-13 11:18:22
*/

$awal  = new DateTime($id);
$akhir = new DateTime(); // Waktu sekarang
$diff  = $awal->diff($akhir);

echo $diff->y . ' Tahun ';
echo $diff->m . ' Bulan ';
echo $diff->d . ' Hari ';
