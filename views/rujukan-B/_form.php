<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:44:52
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-12 00:42:54
*/

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rujukan-form">
    
    <?php $form = ActiveForm::begin([
            'id' => 'login-form-horizontal', 
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
        ]);
    ?>

    <div class="row">
        <div class="col-md-12">
        <div class="panel panel-primary">
              <div class="panel-heading">Data Umum Pasien</div>
              <div class="panel-body">
              
        <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #555555">Pasien Ibu</div>
              <div class="panel-body">
                <?= $form->field($model, 'ibu_nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'ibu_tplahir')->textInput(['maxlength' => true]) ?>

                <?php
                    echo $form->field($model, 'ibu_tglahir')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Pilih Tanggal Lahir', 'id'=>'date1'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ]);
                ?>

                <?= $form->field($model, 'ibu_umur')->textInput() ?>

                <?= $form->field($model, 'ibu_alamat')->textarea(['rows' => 6]) ?>

                <?php
                    $caraBayar = [
                        'UMUM' => 'UMUM',
                        'BPJS' => 'BPJS',
                        'JKD' => 'JKD'
                    ];
                    echo $form->field($model, 'ibu_carabayar')->widget(Select2::classname(), [
                        'data' => $caraBayar,
                        'options' => [
                            'placeholder' => 'Pilih Cara Pembayaran...',
                        ],
                    ]);      
                ?>

              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #555555">Pasien Bayi</div>
              <div class="panel-body">
                <?php
                    $jk = [
                        'Laki-laki' => 'Laki-laki',
                        'Perempuan' => 'Perempuan'
                    ];
                    echo $form->field($model, 'bayi_jk')->radioButtonGroup($jk, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-primary']],
                    ]);
                ?>

                <?= $form->field($model, 'bayi_nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'bayi_tplahir')->textInput(['maxlength' => true]) ?>

                <?php
                    echo $form->field($model, 'bayi_tglahir')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Pilih Tanggal Lahir', 'id'=>'date2'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ]);
                ?>

                <?= $form->field($model, 'bayi_umur')->textInput() ?>

                <?= $form->field($model, 'bayi_alamat')->textarea(['rows' => 6]) ?>

                <?php
                    echo $form->field($model, 'bayi_carabayar')->widget(Select2::classname(), [
                        'data' => $caraBayar,
                        'options' => [
                            'placeholder' => 'Pilih Cara Pembayaran...',
                        ],
                    ]);      
                ?>
              </div>
            </div>
        </div>

        </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
              <div class="panel-heading">Rujukan & Resume Pasien</div>
              <div class="panel-body" style="padding-right: 3.8%">

                <?= Html::activeHiddenInput($model, 'asal_rujukan', ['value' => $model->asal_rujukan]);  ?>
                <?= Html::activeHiddenInput($model, 'tujuan_rujukan', ['value' => $model->tujuan_rujukan]);  ?>

                <?php // $form->field($model, 'asal_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?php // $form->field($model, 'tujuan_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <div class="form-group field-rujukan-asal_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-asal_rujukan">Asal Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->asal_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="form-group field-rujukan-tujuan_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-tujuan_rujukan">Tujuan Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->tujuan_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <?= $form->field($model, 'tindakan_ygdiberikan')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'alasan_rujukan')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'diagnosa')->textarea(['rows' => 6]) ?>

                <?php
                    $kesadaran = [
                        'Sadar' => 'Sadar',
                        'Tidak Sadar' => 'Tidak Sadar'
                    ];
                    echo $form->field($model, 'kesadaran')->radioButtonGroup($kesadaran, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-primary']],
                    ]);
                ?>

                <?= $form->field($model, 'tekanan_darah')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'nadi')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'suhu')->textInput() ?>

                <?= $form->field($model, 'pernapasan')->textInput(['maxlength' => true]) ?>
            
                <div class="form-group field-rujukan-berat_badan">
                    <label class="control-label col-sm-3" for="rujukan-berat_badan">Berat Badan</label>
                    <div class="col-sm-9">
                        <div style="width:45%; min-width:150px;">
                            <?= $form->field($model, 'berat_badan',[
                                    'addon' => [ 
                                        'append' => ['content' => 'Kg', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-tinggi_badan">
                    <label class="control-label col-sm-3" for="rujukan-tinggi_badan">Tinggi Badan</label>
                    <div class="col-sm-9">
                        <div style="width:45%; min-width:150px;">
                            <?= $form->field($model, 'tinggi_badan',[
                                    'addon' => [ 
                                        'append' => ['content' => 'Cm', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>            

                <?php // $form->field($model, 'tinggi_badan')->textInput() ?>

                <?= $form->field($model, 'lila')->textInput(['maxlength' => true]) ?>

                <?php
                    $nyeriOpsi = [
                        'Nyeri Berat' => 'Nyeri Berat',
                        'Nyeri Sedang' => 'Nyeri Sedang',
                        'Nyeri Ringan' => 'Nyeri Ringan'
                    ];
                    echo $form->field($model, 'nyeri')->radioButtonGroup($nyeriOpsi, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-primary']],
                    ]);
                ?>

                <?= $form->field($model, 'keterangan_lain')->textarea(['rows' => 6]) ?>

                <?php echo $form->field($model, 'info_balik')->hiddenInput(['rows' => 6])->label(false) ?>

                <?php // $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
